<?php
require_once 'connection.php';


$link = mysqli_connect($host, $user, $password, $database) or die("Error: " . mysqli_error($link));



// CREATE TABLE family


$sql = "CREATE TABLE IF NOT EXISTS family ( 
    family_id int(10) NOT NULL,
    father_name varchar(36) NULL,
    mother_name varchar(36) NULL,
    PRIMARY KEY (family_id))";

if (mysqli_query($link, $sql)) {
	echo "Table 'family' created successfully";
} else {
	echo "Error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO family VALUES (1, 'Penal', 'Palka'), (2, 'Aen', 'Alla'),
(3, 'Ben', 'Bella')";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'family'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";



// CREATE TABLE club


$sql = "CREATE TABLE IF NOT EXISTS club ( 
    club_id int(10) NOT NULL,
    name varchar(36) NOT NULL,
    PRIMARY KEY (club_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'club' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO club VALUES (1, 'Tver Premium'), (2, 'Atlas'),
(3, 'Kolokol')";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'club'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";


// CREATE TABLE owner


$sql = "CREATE TABLE IF NOT EXISTS owner ( 
    owner_id int(10) NOT NULL,
    last_name varchar(36) NOT NULL,
    first_name varchar(36) NOT NULL,
    patronymic varchar(36) NULL,
    passport varchar(36) NOT NULL,
    club_id int(10) NOT NULL,
    PRIMARY KEY (owner_id),
    FOREIGN KEY (club_id) REFERENCES club (club_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'owner' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO owner VALUES (1, 'Smirnov', 'Denis', 'Andreevich', '4001112233', 2),
(2, 'Tvar', 'Anton', 'Lvovich', '4099142233', 1),
(3, 'Koshkina', 'Alla', 'Artyomovna', '4005146269', 3)";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'owner'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";


// CREATE TABLE dog


$sql = "CREATE TABLE IF NOT EXISTS dog ( 
    dog_id int(10) NOT NULL,
    name varchar(36) NOT NULL,
    breed enum ('dachshund', 'yorkshire terrier', 'irish wolfhound', 'bulldog', 'dalmatian', 'collie', 'corgi', 'german shepherd', 'akita-inu') NOT NULL,
	gender enum ('male', 'female') NOT NULL,
	class enum ('breed', 'pet', 'show') NOT NULL,
	age tinyint (2) UNSIGNED NOT NULL,
	last_vaccination_date date NOT NULL,
	owner_id int (10) NOT NULL,
	family_id int (10) NOT NULL,
	PRIMARY KEY (dog_id),
	FOREIGN KEY (owner_id) REFERENCES owner (owner_id),
	FOREIGN KEY (family_id) REFERENCES family (family_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'dog' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO dog VALUES (1, 'Pip', 'dachshund', 'male', 'breed', 5, '2018-03-09', 1, 1),
(2, 'Jenny', 'yorkshire terrier', 'female', 'pet', 3, '2018-02-27', 2, 2),
(3, 'John', 'corgi', 'male', 'show', 2, '2018-02-27', 2, 3)";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'family'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";

// CREATE TABLE ring


$sql = "CREATE TABLE IF NOT EXISTS ring ( 
    ring_id int(10) NOT NULL,
    start_time time NOT NULL,
	end_time time NOT NULL,
    PRIMARY KEY (ring_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'ring' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO ring VALUES (1, '10:00:00', '16:00:00'),
(2, '11:00:00', '17:00:00'),
(3, '10:00:00', '18:00:00')";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'ring'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";


// CREATE TABLE expert

$sql = "CREATE TABLE IF NOT EXISTS expert ( 
    expert_id int(10) NOT NULL AUTO_INCREMENT,
    last_name varchar (36) NOT NULL,
	first_name varchar (36) NOT NULL,
	patronymic varchar (36) NULL,
	skills_level enum ('high', 'medium', 'starting'),
    PRIMARY KEY (expert_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'expert' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO expert (last_name, first_name, patronymic, skills_level) VALUES ('Dornik', 'Stepan', 'Sergeevich', 'medium'),
('Dornik', 'Aleksei', 'Sergeevich', 'starting'),
('Dornik', 'Sergei', 'Vitalievich', 'high')";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'expert'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";

//CREATE TABLE sponsor

$sql = "CREATE TABLE IF NOT EXISTS sponsor ( 
    sponsor_id int(10) NOT NULL,
    last_name varchar (36) NOT NULL,
	first_name varchar (36) NOT NULL,
	patronymic varchar (36) NULL,
    PRIMARY KEY (sponsor_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'sponsor' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO sponsor VALUES (1, 'Kirov', 'Kirill', 'Olegovich'),
(2, 'Kirovsky', 'Yuri', 'Antonovich'),
(3, 'Stary', 'Szczepan', 'Wlodzimierzewicz')";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'sponsor'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";

//CREATE TABLE dog_show

$sql = "CREATE TABLE IF NOT EXISTS dog_show ( 
    show_id int(10) NOT NULL,
    organisation varchar (36) NOT NULL,
	show_date date NOT NULL,
	show_type enum ('all breeds', 'sole breed') NOT NULL,
	sponsor_id int (10) NOT NULL,
	sponsored_sum int (10) NOT NULL,
    PRIMARY KEY (show_id),
	FOREIGN KEY (sponsor_id) REFERENCES sponsor(sponsor_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'dog_show' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO dog_show VALUES (1, 'Mousetree', '2018-03-14', 'all breeds', 3, 3000000),
(2, 'Callhouse', '2018-03-15', 'all breeds', 2, 1200000),
(3, 'Septown', '2018-03-16', 'all breeds', 1, 1900000)";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'dog_show'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";

//CREATE TABLE action

$sql = "CREATE TABLE IF NOT EXISTS action ( 
    dog_id int(10) NOT NULL,
	show_id int(10) NOT NULL,
	ring_id int(10) NOT NULL,
	expert_id int(10) NOT NULL,
	points_1 tinyint (3) NOT NULL,
	points_2 tinyint (3) NOT NULL,
	points_3 tinyint (3) NOT NULL,
    PRIMARY KEY (dog_id, show_id),
	FOREIGN KEY (dog_id) REFERENCES dog(dog_id),
	FOREIGN KEY (show_id) REFERENCES dog_show(show_id),
	FOREIGN KEY (ring_id) REFERENCES ring(ring_id),
	FOREIGN KEY (expert_id) REFERENCES expert(expert_id))";

if (mysqli_query($link, $sql)) {
    echo "Table 'action' created successfully";
} else {
    echo "Error: " . mysqli_error($link);
}
echo "<br>";


$sqql = "INSERT INTO action VALUES (1, 1, 1, 1, 88, 50, 23),
(2, 1, 2, 3, 38, 45, 44), (3, 1, 3, 2, 89, 26, 2)";

if ($link->query($sqql) === TRUE) {
    echo "New record created successfully in 'action'";
} else {
    echo "Error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";
?>