<?php
require_once 'connection.php';

$link = mysqli_connect($host, $user, $password, $database)
or die("Error: " . mysqli_error($link));

// variables

$club_name = strtr($_GET['club_name'], '*', '%');
$last_name = strtr($_GET['last_name'], '*', '%');

// form

echo "<b>DOG SEARCH</b><br>
<form method='GET' action='search.php'>
<p>Choose club name: <input type='text' name='club_name' value='$club_name'></p>
<p>The expert's last name: <input type='text' name='last_name' value='$last_name'></p>
<p><input type='submit' name='enter' value='Search'></p>
</form>";

if (isset($_GET['enter'])) {

$sql = "SELECT d.name AS dog_name, d.breed AS breed, c.name AS club_name, e.last_name AS last_name
	FROM dog AS d, action AS a, expert AS e, owner AS o, club AS c
	WHERE a.dog_id = d.dog_id AND a.expert_id = e.expert_id
	AND d.owner_id = o.owner_id AND o.club_id = c.club_id
	AND e.last_name LIKE '%$last_name%' AND c.name LIKE '%$club_name%'";

$qq = mysqli_query($link, $sql);

echo "<table border='1'>
<tr> 
<th>dog</th>
<th>breed</th>
<th>club</th>
<th>expert</th>
</tr>";

while($row = mysqli_fetch_array($qq)) {
echo "<tr>";
echo "<td>" . $row['dog_name'] . "</td>";
echo "<td>" . $row['breed'] . "</td>";
echo "<td>" . $row['club_name'] . "</td>";
echo "<td>" . $row['last_name'] . "</td>";
echo "</tr>";
}

echo "</table>"; 
}

mysqli_close($link);
?>